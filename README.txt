What is this module?
==============
The Subtheme module allow you upload a zip file contain css, js and images files
and override the css settings of your drupal site.

What is the use case of that?
===============
A more specific use case is when using OG. OG is a module providing to users
groups that they can mange and control a content access on a drupal site.

If a group admin would like to override the global css settings for his specific
group, the Subtheme module will provide the solution(The module OG context will
be needed for that).

How to use the module
===============
Go to admin/structure/subtheme. Click on "Add a new subtheme", assert a title to
your new theme, upload a zip file and submit the form. All of the themes you'll
upload will be added on each hook init.

Files structure
===============
The css files will need to be located under a folder named "css"
The js files will need to be located under a folder named "js".

You will not need to declare them or any thing similar to that, all of the js
or css files located under their folder will be loaded automatically.

Info files will be load also automatically but they need to be located in the
same level of the css/js folder. You can have a look on the next structure for
example:

+---------------------+
| Example folder      |
+---------------------+
+- css (folder)       |
+--- foo.css (file)   |
+--- bar.css (file)   |
+---                  |
+- js (folder)        |
+--- foo.js (file)    |
+--- bar.js (file)    |
+---                  |
+- foo.info (file)    |
+---------------------+

Your info files holds the settings for the css/js files. The settings for each
file will be in a format of the drupal_add_js/drupal_add_css option arguments.
For example you take a look at the next definition:
foo.css[browsers][!IE] = FALSE

This a settings that relate to the foo.css file and handle the browser settings.
No path in the file name is needed, just the name.

A dummy subtheme is attached to the module name "subtheme-example.zip".
