
(function ($) {

  Drupal.behaviors.nodeFieldsetSummaries = {
    attach: function (context) {
      $('fieldset.subtheme-form-owner', context).drupalSetSummary(function (context) {
        var name = $('.form-item-name input', context).val();

        return Drupal.t('By @name', { '@name': name });
      });
    }
  };

})(jQuery);
