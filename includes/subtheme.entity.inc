<?php
/**
 * @file
 * Sub theme entity class handler.
 */
class SubTheme extends Entity {

  /**
   * The ID of the sub theme entity.
   * @var
   */
  public $id;

  /**
   * The title of the sub theme.
   * @var
   */
  public $title;

  /**
   * The bundle of the sub theme.
   * @var
   */
  public $type;

  /**
   * The unix timestamp the sub theme created.
   * @var
   */
  public $timestamp;

  /**
   * The status of the sub theme.
   * @var
   */
  public $status;

  /**
   * The file ID of the sub theme.
   * @var
   */
  public $fid;

  /**
   * Store the path for the extracted zip files.
   * @var
   */
  public $path;

  /**
   * The owner of the sub theme.
   * @var
   */
  public $uid;

  /**
   * An instance of this class - store the info for the file.
   * @var
   */
  public $file;

  /**
   * Settings for the drupal_add_css function.
   * @var
   */
  public $cssSettings;

  /**
   * Settings for the drupal_add_js function.
   * @var
   */
  public $jsSettings;

  /**
   * The file path of the ZIP file.
   * @var
   */
  public $filePath;

  /**
   * The path the zip file should extract to.
   * @var
   */
  public $extractPath;

  /**
   * Flag variable for update the entity later.
   * @var
   */
  public $updated = FALSE;

  /**
   * Overriding __construct().
   */
  function __construct($values) {
    $values += array(
      'created' => time(),
      'updated' => time(),
    );

    parent::__construct($values, 'subtheme');
  }

  /**
   * Overriding save().
   */
  public function save() {
    $this->updated = time();

    parent::save();
  }

  /**
   * Change the settings for drupal_add_css function.
   *
   * @param $options
   *  Settings to be extended to the drupal_add_css function.
   *
   * @return SubTheme
   * @see drupal_add_css().
   */
  public function changeCssSettings($options) {
    $options += array(
      'group' => CSS_THEME,
      'weight' => 0,
      'every_page' => FALSE,
      'media' => 'all',
      'preprocess' => TRUE,
      'browsers' => array(),
    );

    $this->cssSettings = $options;

    return $this;
  }

  /**
   * Change the settings for drupal_add_js function.
   *
   * @param $options
   *  Settings to be extended to the drupal_add_js function.
   *
   * @return SubTheme
   * @see drupal_add_css().
   */
  public function changeJsSettings($options) {
    $options += drupal_js_defaults(NULL);

    $this->jsSettings = $options;

    return $this;
  }

  /**
   * Add the css file.
   */
  public function addAssets() {
    if (!$this->path) {
      // Subtheme not extracted properly.
      return;
    }

    $css_files = glob($this->path . '/css/*.css');
    $js_files = glob($this->path . '/js/*.js');

    $info = $this->parseInfo();

    // Adding CSS files.
    foreach ($css_files as $css_file) {
      // Get the file name of the css file.
      $file = $this->getFileNameFromPath($css_file);

      // Get the settings for the file from the info file and add them.
      if (!empty($info[$file])) {
        $this->changeCssSettings($info[$file]);
      }

      drupal_add_css($css_file, $this->cssSettings);
    }

    // Adding JS files.
    foreach ($js_files as $js_file) {
      // Get the file name of the css file.
      $file = $this->getFileNameFromPath($js_file);

      // Get the settings for the file from the info file and add them.
      if (!empty($info[$file])) {
        $this->changeJsSettings($info[$file]);
      }

      drupal_add_js($js_file, $this->jsSettings);
    }

    return $this;
  }

  /**
   * Get the file name from the path.
   */
  public function getFileNameFromPath($path) {
    $data = explode('/', $path);

    return end($data);
  }

  /**
   * Return info for the intended folder path for a zip file.
   */
  public function fileInfo() {
    $this->file = file_load($this->fid);

    if (!$this->fid && $this->path) {
      // We don't have a file id but we do have a path, no action is needed.
      return;
    }

    // Creating an array of stream wrappers that will be removed.
    $streams = array();
    foreach (stream_get_wrappers() as $stream) {
      $streams[] = $stream . '://';
    }

    // Generate the folder name by the unique URI of the file.
    $file_name = str_replace($streams, '', $this->file->uri);
    $folder_name = str_replace(array('.', '_'), '-', $file_name);

    $files_folder = variable_get('file_public_path', conf_path() . '/files');

    $this->filePath = $files_folder . '/' . $file_name;
    $this->extractPath = $files_folder . '/' . $folder_name;
  }

  /**
   * Extracting the zip file.
   */
  public function extract() {
    $this->fileInfo();

    if (!$this->fid && $this->path) {
      // We don't have a file id but we do have a path, no action is needed.
      return;
    }

    $zip = new ZipArchive;
    $res = $zip->open($this->filePath);

    if ($res === TRUE) {
      // Extract the file.
      $zip->extractTo($this->extractPath);
      $zip->close();
      $this->path = $this->extractPath;

      // The path updated, set the flag variable to true.
      $this->updated = TRUE;
    }
    else {
      // couldn't extract the zip file, display an error message and watchdog.
      $params = array(
        '@file' => $this->filePath,
        '@path' => $this->extractPath,
      );

      $message = t('Unable to extract @file to @path', $params);
      watchdog('subtheme', $message);
      drupal_set_message($message, 'error');
    }
  }

  /**
   * Parsing the info file of the sub theme.
   */
  public function parseInfo() {
    $info_files = glob($this->path . '/*.info');

    $info = array();

    // Get info files.
    foreach ($info_files as $info_file) {
      // Get the information from the info file.
      $file_content = drupal_parse_info_file($info_file);

      foreach ($file_content as $file => $content) {
        $info[$file] = $content;
      }
    }

    return $info;
  }
}
